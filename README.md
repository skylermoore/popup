![Popup screenshot](Screenshot.png "Popup screenshot")

## Intended project goals

the goal of popup is to provide a similar utility to dialog/whiptail, but focus on X11 and DRM rendering. Full gamepad support with the ability to customize both look and feel are planned. The intended use is to provide user facing scripts the ability to prompt for input even when outside of a desktop environment, or without a keyboard/mouse. Popup should remain lightweight and easy to use for all scripting environments.

Check the [Wiki](https://gitlab.com/skylermoore/popup/-/wikis/home) for more information on supported Feature and build instructions.

Check the examples directory for example bash scripts for each popup type.

## Quickstart for Debian/Ubuntu:

Install needed packages:
```Shell
sudo apt install build-essential git meson ninja cmake libasound2-dev mesa-common-dev libx11-dev libxrandr-dev libxi-dev xorg-dev libgl1-mesa-dev libglu1-mesa-dev
```

Clone with submodules
```Shell
git clone --recurse-submodules https://gitlab.com/skylermoore/popup.git
```

Create build directory, and compile the project
```Shell
meson build
cd build
meson compile
```
