enum return_status {
	STATUS_OK = 0,
	STATUS_CANCEL = 1,
	STATUS_ERROR = 255
};

void drawBorder(void) {
	// Draw title if set, or draw outer box
	if(param.title) {
		GuiSetStyle(DEFAULT, TEXT_SIZE, param.tsize);
		GuiGroupBox((Rectangle){
				param.tsize/2, param.tsize/2, // Top left corner
				GetScreenWidth()-param.tsize, GetScreenHeight()-param.tsize},
				param.title);
		GuiSetStyle(DEFAULT, TEXT_SIZE, param.fsize);
	} else {
		GuiPanel((Rectangle){5,5,GetScreenWidth()-10,GetScreenHeight()-10}, NULL);
	}
}

void drawMessageBox(const char *message) {
	Rectangle messageBounds = {
		param.tsize, param.tsize, // Top left corner
		GetScreenWidth()-param.tsize*2, GetScreenHeight()-param.tsize*2};

	//NOTE: From old versions of Raylib. See Raylib_modules.h
	DrawTextBoxed(GetFontDefault(), message, messageBounds, param.fsize, 1, true, 
			GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_NORMAL)));
}

int buttonHeight() {
	return GetScreenHeight()-param.tsize-param.fsize*2;
}

