enum mode {
	MODE_NONE,
	MODE_MSGBOX,
	MODE_YESNO,
	MODE_INPUTBOX,
	MODE_GUAGE,
	MODE_MENU
};

static struct params {
	int mode;
	int screenWidth;
	int screenHeight;
	const char *title;
	int fsize;
	int tsize;
	int gamepad;
	bool fullscreen;
	const char *range;
} param;

void print_usage(void) {
	puts("popup <mode> <data> [FLAGS]...");
	puts("modes: msgbox, yesno, guage, inputbox, menu");
	kgflags_print_errors();
	kgflags_print_usage();
}

int parse_args(int argc, char **argv) {
	kgflags_int("width", 800, "Window width", false, &param.screenWidth);
	kgflags_int("height", 450, "Window height", false, &param.screenHeight);
	kgflags_int("fsize", 10, "Font size", false, &param.fsize);
	kgflags_int("tsize", 10, "Title size", false, &param.tsize);
	kgflags_int("gamepad", 0, "Gamepad to use for control", false, &param.gamepad);

	kgflags_string("title", NULL, "Popup title", false, &param.title);
	kgflags_string("range", "0-100", "Guage range: 0-100", false, &param.range);

	kgflags_bool("fullscreen", false, "Fullscreen Mode", false, &param.fullscreen);

	kgflags_set_custom_description("Optional flags:");
	kgflags_set_prefix("--");
	if(!kgflags_parse(argc, argv)) {
		return 1;
	}

	const char *command = argv[0];

	int calling_length = TextLength(argv[0]);
	if(calling_length >= 5 && TextIsEqual(argv[0]+calling_length-5, "popup")) {
		if(kgflags_get_non_flag_args_count())
			command = kgflags_get_non_flag_arg(0);
	}

	if(TextIsEqual(command, "msgbox")) {
		param.mode = MODE_MSGBOX;
	} else if(TextIsEqual(command, "yesno")) {
		param.mode = MODE_YESNO;
	} else if(TextIsEqual(command, "inputbox")) {
		param.mode = MODE_INPUTBOX;
	} else if(TextIsEqual(command, "guage")) {
		param.mode = MODE_GUAGE;
	} else if(TextIsEqual(command, "menu")) {
		param.mode = MODE_MENU;
	} else {
		param.mode = MODE_NONE;
		return 1;
	}

	return 0;
}
