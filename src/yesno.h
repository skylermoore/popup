int yesno(void) {
	enum hover_options {
		HOVERED_YES,
		HOVERED_NO
	};

	// First argument is message to display
	const char *message = kgflags_get_non_flag_arg(1);

	if(!message)
		return STATUS_ERROR;

	int widthCenter = GetScreenWidth()/2;
	// Height buttons should be drawn at
	int selection = HOVERED_YES;

	while(!WindowShouldClose()) {
		// Logic
		widthCenter = GetScreenWidth()/2;

		// Navigation
		switch(direction()) {
			case DIRECTION_LEFT:
				selection = HOVERED_YES;
				break;
			case DIRECTION_RIGHT:
				selection = HOVERED_NO;
				break;
		}

		// Selection
		switch(control()) {
			case CONTROL_SELECT:
			case CONTROL_SUBMIT:
				return selection;
		}

		// Rendering
		BeginDrawing();

		ClearBackground(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)));

		drawBorder();

		drawMessageBox(message);

		// Draw buttons
		if(selection == HOVERED_YES)
			GuiSetState(STATE_FOCUSED);
		else
			GuiSetState(STATE_NORMAL);
		if(GuiButton((Rectangle){
				widthCenter-param.fsize*4-param.tsize, buttonHeight(), // Top Left Corner
				param.fsize*4, param.fsize*2},
				"Yes"))
			return STATUS_OK;

		if(selection == HOVERED_NO)
			GuiSetState(STATE_FOCUSED);
		else
			GuiSetState(STATE_NORMAL);
		if(GuiButton((Rectangle){
				widthCenter+param.tsize, buttonHeight(), // Top Left Corner
				param.fsize*4, param.fsize*2},
				"No"))
			return STATUS_CANCEL;

		EndDrawing();
	}

	return STATUS_ERROR;
}
