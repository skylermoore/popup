#include <unistd.h>

#include "raylib.h"
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
#undef RAYGUI_IMPLEMENTATION

#define KGFLAGS_IMPLEMENTATION
#include "kgflags.h"
#undef KGFLAGS_IMPLEMENTATION

#include "args.h"
#include "raylib_modules.h"
#include "common.h"
#include "controls.h"
#include "guage.h"
#include "inputbox.h"
#include "menu.h"
#include "msgbox.h"
#include "yesno.h"

int main(int argc, char **argv) {
	if(parse_args(argc, argv)) {
		print_usage();
		return STATUS_ERROR;
	}

	if(param.mode != MODE_NONE) {
		SetTraceLogLevel(LOG_WARNING); // Disable info logs from raylib
		InitWindow(param.screenWidth, param.screenHeight,
				param.title ? param.title : "Popup");
		if(param.fullscreen && !IsWindowFullscreen())
			ToggleFullscreen();
		SetTargetFPS(60);
		GuiSetStyle(DEFAULT, TEXT_SIZE, param.fsize);
	}

	int result = STATUS_ERROR;

	switch(param.mode) {
		case MODE_MSGBOX:
			result = msgbox();
			break;
		case MODE_YESNO:
			result = yesno();
			break;
		case MODE_INPUTBOX:
			result = inputbox();
			break;
		case MODE_GUAGE:
			result = guage();
			break;
		case MODE_MENU:
			result = menu();
			break;
	}

	if(GetWindowHandle())
		CloseWindow();

	return result;
}
