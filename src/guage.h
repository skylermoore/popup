#define PROGRESS_SIZE 1024 // STDIN char buffer size

int guage(void) {
	float progress = 0;
	// Stdio input buffer for bar progress
	char buf[PROGRESS_SIZE] = {0};
	// Holds the processed input from stdin
	const char **input = NULL;

	// Read range parameter. The default is 0-100 as defined in args.h
	int splitCount = 0;
	const char **range = TextSplit(param.range, '-', &splitCount);
	if(splitCount != 2)
		return STATUS_ERROR;

	int low = TextToInteger(&range[0][0]);
	int high = TextToInteger(&range[1][0]);

	while(!WindowShouldClose()) {
		// Render first because Logic is blocking on stdin. While we are
		// technically rendering a window with no widgets, this allows slow
		// outputting applications to not cause a black window until stdin is
		// recieved.
		// Rendering
		BeginDrawing();

		ClearBackground(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)));

		drawBorder();

		if(input)
			drawMessageBox(input[0]);

		Rectangle guageBounds = {
			param.tsize, GetScreenHeight()-param.tsize*2-param.fsize*2, // Top left corner
			GetScreenWidth()-param.tsize*2, param.fsize*2};

		GuiProgressBar(guageBounds, NULL, NULL, &progress, low, high);

		EndDrawing();

		// TODO: Move to non-blocking input so rendering can continue.
		// Logic
		size_t bytes = read(0, buf, sizeof(buf));
		buf[bytes-1] = '\0'; // Replace new line with null termination
		int count;
		input = TextSplit(buf, ';', &count);
		if(count > 1)
			progress = TextToInteger(input[1]);
		if(progress == high || bytes == 0)
			return STATUS_OK;
	}

	return STATUS_ERROR;
}
