int menu(void) {
	// First argument is message to display
	const char *message = kgflags_get_non_flag_arg(1);
	// Second argument is menu options
	const char *menu = kgflags_get_non_flag_arg(2);

	if(!message || !menu)
		return STATUS_ERROR;

	// Passed in menu options
	int numOptions = 0;
	const char **options = TextSplit(menu, ';', &numOptions);

	if(numOptions < 1)
		return STATUS_ERROR;

	int scrollIndex = 0;
	int selected = 0;
	int old_selection = 0;
	int focused = -1;
	bool updateView = false;

	GuiSetState(STATE_NORMAL);

	while(!WindowShouldClose()) {
		// Directions
		switch(direction()) {
			case DIRECTION_UP:
				selected = --selected < 0 ? 0 : selected;
				updateView = true;
				break;
			case DIRECTION_DOWN:
				// Equal to numOptions is used to handle selecting Cancel button
				selected = ++selected >= numOptions ? numOptions : selected;
				updateView = true;
				break;
		}

		// Controls
		switch(control()) {
			submit:
			case CONTROL_SELECT:
			case CONTROL_SUBMIT:
				if(selected == numOptions)
					return STATUS_CANCEL;
				puts(options[selected]);
				return STATUS_OK;
		}

		// Need to calcuate these here since they change with window size
		int scrollAreaStart = GetScreenHeight()/2;
		int scrollAreaHeight = buttonHeight()/2-param.tsize-param.fsize;
		int maxVisible = scrollAreaHeight/(GuiGetStyle(LISTVIEW, LIST_ITEMS_SPACING) + GuiGetStyle(LISTVIEW, LIST_ITEMS_HEIGHT));

		// If keyboard input was used to control the menu, and we aren't hovering
		// cancel.
		if(updateView && selected != numOptions) {
			if(selected > scrollIndex+maxVisible-1)
				scrollIndex = selected-maxVisible+1;
			else if(selected < scrollIndex)
				scrollIndex = selected;
		}
		updateView = false;

		// Rendering
		BeginDrawing();

		ClearBackground(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)));

		drawBorder();

		drawMessageBox(message);

		GuiSetState(STATE_NORMAL);

		old_selection = selected;
		// Treat the clicking of an option with the mouse as hitting submit on
		// the keyboard. Update selected, and jump to submit code.
		int newSelection = GuiListViewEx((Rectangle){param.tsize, scrollAreaStart,
				GetScreenWidth()-param.tsize*2, scrollAreaHeight}, 
				options, numOptions, &scrollIndex, &selected, &focused);

		if(selected != old_selection) {
			goto submit;
		}

		if(selected == numOptions)
			GuiSetState(STATE_FOCUSED);
		else
			GuiSetState(STATE_NORMAL);

		// Treat the clicking of cancel with the mouse as hitting submit on
		// the keyboard.
		if(GuiButton((Rectangle){
				GetScreenWidth()/2-param.fsize*2, buttonHeight(), // Top Left Corner
				param.fsize*4, param.fsize*2},
				"Cancel")) {
			selected = numOptions;
			goto submit;
		}

		EndDrawing();
	}

	return STATUS_ERROR;
}
