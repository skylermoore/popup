int msgbox(void) {
	// First argument is message to display
	const char *message = kgflags_get_non_flag_arg(1);

	if(!message)
		return STATUS_ERROR;

	// We only render one element this effects, no need to set in loop
	GuiSetState(STATE_FOCUSED);

	while(!WindowShouldClose()) {
		// Controls
		switch(control()) {
			case CONTROL_SELECT:
			case CONTROL_SUBMIT:
				return STATUS_OK;
		}

		// Rendering
		BeginDrawing();

		ClearBackground(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)));

		drawBorder();

		drawMessageBox(message);

		// Draw OK button
		if(GuiButton((Rectangle){
				GetScreenWidth()/2-param.fsize*2,
				GetScreenHeight()-param.tsize-param.fsize*2, // Top Left Corner
				param.fsize*4, param.fsize*2},
				"Ok"))
			return STATUS_OK;

		EndDrawing();
	}

	return STATUS_ERROR;
}
