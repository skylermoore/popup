enum directions {
	DIRECTION_NONE,
	DIRECTION_LEFT,
	DIRECTION_RIGHT,
	DIRECTION_UP,
	DIRECTION_DOWN
};

enum controls {
	CONTROL_NONE,
	CONTROL_SELECT,
	CONTROL_SUBMIT,
	CONTROL_BACK
};

int direction(void) {
	if(IsKeyPressed(KEY_LEFT))
		return DIRECTION_LEFT;
	if(IsKeyPressed(KEY_RIGHT))
		return DIRECTION_RIGHT;
	if(IsKeyPressed(KEY_UP))
		return DIRECTION_UP;
	if(IsKeyPressed(KEY_DOWN))
		return DIRECTION_DOWN;

	if(IsGamepadAvailable(param.gamepad)) {
		if(IsGamepadButtonPressed(param.gamepad, GAMEPAD_BUTTON_LEFT_FACE_LEFT))
			return DIRECTION_LEFT;
		if(IsGamepadButtonPressed(param.gamepad, GAMEPAD_BUTTON_LEFT_FACE_RIGHT))
			return DIRECTION_RIGHT;
		if(IsGamepadButtonPressed(param.gamepad, GAMEPAD_BUTTON_LEFT_FACE_UP))
			return DIRECTION_UP;
		if(IsGamepadButtonPressed(param.gamepad, GAMEPAD_BUTTON_LEFT_FACE_DOWN))
			return DIRECTION_DOWN;
	}

	return DIRECTION_NONE;
}

int control(void) {
	if(IsKeyPressed(KEY_ENTER))
		return CONTROL_SUBMIT;

	if(IsKeyPressed(KEY_SPACE)
			|| IsGamepadButtonPressed(param.gamepad,
				GAMEPAD_BUTTON_RIGHT_FACE_DOWN))
		return CONTROL_SELECT;

	return CONTROL_NONE;
}
