#define INPUT_SIZE 255 // input box buffer size

int inputbox(void) {
	enum hover_options {
		HOVERED_INPUT,
		HOVERED_BUTON
	};
	
	// First argument is message to display
	const char *message = kgflags_get_non_flag_arg(1);

	if(!message)
		return STATUS_ERROR;

	char input[INPUT_SIZE] = {0};
	// We need to shift the input pointer around so it renders properlly. This
	// is the offset we use for that.
	int offset = 0;
	int selection = HOVERED_INPUT;

	while(!WindowShouldClose()) {
		// Controls
		switch(direction()) {
			case DIRECTION_UP:
				selection = HOVERED_INPUT;
				break;
			case DIRECTION_DOWN:
				selection = HOVERED_BUTON;
				break;
		}

		switch(control()) {
			case CONTROL_SUBMIT:
				puts(input);
				return STATUS_OK;
		}

		// Rendering
		BeginDrawing();

		ClearBackground(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)));

		drawBorder();

		drawMessageBox(message);

		// Input box on bottom screen, Leaving room for button
		Rectangle inputBounds = {
			param.tsize, GetScreenHeight()-param.tsize*2-param.fsize*4, // Top left corner
			GetScreenWidth()-param.tsize*2, param.fsize*2};

		int maxRenderWidth = (inputBounds.width-GuiGetStyle(TEXTBOX, TEXT_INNER_PADDING)*2);
		int textRendersize = GetTextWidth(input+offset)+GuiGetStyle(DEFAULT, TEXT_SIZE)*2;

		// If the text to be rendered is to large, offset the pointer
		//TODO(Skyler): Calculate offset based on entire input size
		if(textRendersize >= maxRenderWidth)
			offset += (textRendersize-maxRenderWidth)/GuiGetStyle(DEFAULT, TEXT_SIZE);
		else if (offset > 0)
			offset -= (maxRenderWidth-textRendersize)/GuiGetStyle(DEFAULT, TEXT_SIZE);

		// If the window is resized to fast, offset could be out of bounds
		if(offset < 0)
			offset = 0;
		else if(offset >= INPUT_SIZE)
			offset = INPUT_SIZE-1;

		if(selection == HOVERED_INPUT)
			GuiSetState(STATE_FOCUSED);
		else
			GuiSetState(STATE_DISABLED);
		GuiTextBox(inputBounds, input+offset, INPUT_SIZE-offset, true);

		if(selection == HOVERED_BUTON)
			GuiSetState(STATE_FOCUSED);
		else
			GuiSetState(STATE_NORMAL);
		if(GuiButton((Rectangle){
				GetScreenWidth()/2-param.fsize*2,
				buttonHeight(), // Top Left Corner
				param.fsize*4, param.fsize*2},
				"Submit")){
			puts(input);
			return STATUS_OK;
		}

		EndDrawing();
	}

	return STATUS_ERROR;
}
