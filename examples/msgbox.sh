#!/bin/bash

./../build/popup msgbox \
	"Basic message box"

./../build/popup msgbox \
	--title "Test Title" \
	"This is a test message box"

./../build/popup msgbox \
	--title "Large Font Box" \
	--tsize 20 \
	"This is a larger message box" \
	--fsize 20

./../build/popup msgbox \
	--title "Word Wrap Test" \
	"This is a long message with a large font. This text should auto wrap." \
	--fsize 20 \
	--width 200 \
	--height 200
