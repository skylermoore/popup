#!/bin/bash

download() {
	wget "$1" --progress=bar -q --show-progress 2>&1 | stdbuf -oL awk '/%/{gsub("%", "", $(NF-2));printf("Downloading...;%s\n",$(NF-2));fflush()}'
}

download "http://lg-dal.racknerd.com/25MB.test" | ../build/popup guage --fsize 20
