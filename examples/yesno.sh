#!/bin/bash

likesIcecream=$(./../build/popup yesno \
	--title "Ice cream?" \
	"Do you like ice cream?" \
	--tsize 20 \
	--fsize 40)

if [ $? -eq 0 ]; then
	echo "Me too!!!"
else
	echo "How do you not like ice cream?"
fi
