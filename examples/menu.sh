#!/bin/bash

flavor=$(./../build/popup menu \
	--title "Favorite Ice cream Flavor" \
	--fsize 20 \
	"What is your favorite ice cream flavor?" \
	"Vanilla;Chocolate;Strawberry;Rocky Road;Birthday Cake;Fudge;Other"
	)

if [ $? -eq 0 ]; then
	echo "I also enjoy $flavor"

	case $flavor in
		"Vanilla")
			echo "It's my favorite";;
		"Chocolate")
			echo "Not the best, but a close second";;
		"Strawberry")
			echo "Not my top choice";;
		"Rocky Road")
			echo "Can be really good sometimes";;
		"Birthday Cake")
			echo "Childhood memories";;
		"Fudge")
			echo "Yes please!!!";;
		"Other")
			echo "I'm sure it tastes amazing!";;
	esac
else
	echo "You closed the window :("
fi

