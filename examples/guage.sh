#!/bin/bash

# Counter for stdin test
counter() {
	count=0
	while [ $count -le 100 ]; do
		echo "Counting up;$count"
		sleep .1
		count=$((count + 1))
	done
}

# Counter for stdin test
stages() {
	echo "Just getting started;0"
	sleep 2
	echo "Getting warmer;25"
	sleep 2
	echo "Half way there;50"
	sleep 2
	echo "Almost done;75"
	sleep 2
	echo "Goodbye;100"
}

counter | ../build/popup guage --fsize 20
stages | ../build/popup guage --fsize 20
