#!/bin/bash

name=$(./../build/popup inputbox \
	--title "Name" \
	--fsize 20 \
	"Please type your name")

if [ $? -eq 0 ]; then
	echo "Your name is: $name"
else
	echo "You closed the window :("
fi
